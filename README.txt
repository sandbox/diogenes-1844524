It is anticipated that the When Pigs Fly (WPF) module will become part of Drupal 
core for the special WPF release of Drupal.  No release date has been set.

In the meantime, use this module for instructional purposes on "How NOT to Use 
Drupal" or even "Practical Jokes for Drupal Developers -- How to Drive Your 
Colleagues Crazy".

WPF is part of Aardvark package, a shameless attempt to be at the top of the
Modules list, next to Core. That way this code has less far to travel once it has 
been invited to join core. Location, location, location.

The Aardvark package also includes the "Baboons Beware" (BB) module, which is 
almost exactly the same as WPF, except different.

There are no comments to be found in the code. Trust me, it's better this way.

*How NOT to use WPF*

1) Unzip the package. There are two modules. WPF and BB. Have a look at the
   code. Each module creates a page that will appear at the /whenpigsfly url. 
   Enable one, or the other, or both modules, and observe what happens.
   
2) Now create some content. An Article or Basic Page will do. Give it a 
   url alias of 'whenpigsfly'.  Publish it and promote to the front page. 
   Observe what happens.
   
3) Change the url alias in Step 2 to something absurd like /admin/people/accounts
   Observe what happens. Now reverse this back to 'whenpigsfly' as in Step 2.
   
4) Assuming you are an administrator with the powers of a god, create a 
   subdirectory in your Drupal root directory. Call it 'whenpigsfly'.  Copy the
   index.php file included with either module to this directory. Observe what 
   happens. It might look similar to the pages found at
    
   http://localhost/all/modules/wpf/ 
   or at
   http://localhost/all/modules/baboons/
   
   or whatever localhost is -- multi-site configurations  offer all kind of 
   possibilities. If a proper .htaccess file was included, this would not happen. 
   Oh well.

5) Think about this. Hack the code. Have some fun. And be aware of possible URL 
   conflicts that can happen with Drupal.

NOTES:

The help_hook() implementation is technically Help, though it may NOT be much 
help.  Once again, this code is for instructional purposes only.

This code should probably require the 'Bad Judgement' module. Requesting a 
co-maintainer to argue endlessly about this. Or a module that will figure it out
automatically.
